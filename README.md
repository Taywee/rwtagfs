# rwtagfs

A read-write tag filesystem, which takes ownership of the files you put in it,
but is also usable in a read-write context (modifying files should be done
quickly in place), and is entirely used via its fuse filesystem.

## The motivation

I really like the idea of tag filesystems, but I haven't found one I really
like.  TMSU is pretty much link-based, and doesn't take ownership of the files,
meaning I have to organize them and generally keep them on the filesystem
anyway.  Tagsistant stores files based on their hash, and editing files and
keeping open read-write file handles to files is problematic.

I want a tag filesystem that:

* Takes ownership of its files, but does not key them by their hash.
* Deduplicates files if two files are equal.
* Exposes triple tags, with an optional namespace.
* Allows full querying through the filesystem without spaces (and ideally with
  customizable strings).
* Also allows complex querying through special filesystem files (maybe also
  offering a command line to interface with these files automatically).
* Allows editing files in place efficiently.
* Allows optional in-line compression.
* Allows storing directories.

The general idea:

* The filesystem is only officially used through mounting it.  It's a tag
  filesystem first and foremost.
* The filesystem stores its files internally, as a series of blocks in the
  SQLite database.
  * Each block may be individually compressed, so files may have different
    compression formats, and mount compression format can be changed between
    mounts, or even file to file.
  * Each block stores its (uncompressed) checksum as well.
  * Each file also stores its checksum, which is not the checksum of the file
    data, but the checksum of the concatenation of all its blocks' checksums.
    This sum is used to deduplicate files.
  * Deduplication is checked when a file is closed.
* Directories may be stored directly and tagged.  Stored directories use
  concepts from rwarchivefs.  They might not be able to be deduplicated, but
  we'll see.
* As much as possible is done inside of SQLite triggers and transactions.
* Complex querying and scripting can be done on a tagset-by-tagset or
  file-by-file basis.
